/**
 * Server side code.
 */
"use strict";
console.log("Starting...");
var express = require("express");
var bodyParser = require("body-parser");
var moment = require('moment');
moment().format();

var app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

console.log(__dirname);
console.log(__dirname + "/../client/");
const NODE_PORT = process.env.PORT || 3000;

app.use(express.static(__dirname + "/../client/"));

app.post("/api/register", (req, res)=>{
    var registeredUser = req.body;
    console.log("Email > " + registeredUser.email);
    console.log("password > " + registeredUser.password);
    console.log("confirmpassword > " + registeredUser.confirmpassword);
    console.log("fullName > " + registeredUser.fullName);
    console.log("gender > " + registeredUser.gender);
    console.log("Date of birth > " + registeredUser.dob);
    var newDob = moment(registeredUser.dob);
    console.log(newDob.toLocaleString());
    console.log("fullName > " + registeredUser.address);
    console.log("selectedNationality > " + registeredUser.selectedNationality);
    console.log("contactNumber > " + registeredUser.contactNumber);
    registeredUser.message = "THANK YOU !";
    res.status(200).json(registeredUser);
});

app.use(function (req, res) {
    res.send("<h1>Page not found</h1>");
});

app.listen(NODE_PORT, function () {
    console.log("Web App started at " + NODE_PORT);
});

//make the app public. In this case, make it available for the testing platform
module.exports = app
